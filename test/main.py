


def main():
    import machine
    pin16 = machine.Pin(16, machine.Pin.OUT)
    pin16.value(1)
    import machine, ssd1306
    i2c = machine.I2C(scl=machine.Pin(15), sda=machine.Pin(4))
    oled = ssd1306.SSD1306_I2C(128, 64, i2c)
    oled.fill(0)
    oled.text('MicroPthonForum:', 0, 0)
    oled.show()


if __name__ == '__main__':
    print("running")
    main()