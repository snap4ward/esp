import utime
from machine import Pin


def main():
    led2 = Pin(2, Pin.OUT)
    led1 = Pin(16,Pin.OUT)
    enabled = False
    while True:
        if enabled:
            led2.off()
            led1.on()
            print("off")
        else:
            led2.on()
            led1.off()
            print("on")
        utime.sleep_ms(600)
        enabled = not enabled


if __name__ == '__main__':
    print("running")
    main()