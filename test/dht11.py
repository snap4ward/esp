import dht
import machine
while True:
    d = dht.DHT11(machine.Pin(4))
    d.measure()
    temp = d.temperature
    humid = d.humidity()
    print("temp: ",temp," humidity: ",humid)
